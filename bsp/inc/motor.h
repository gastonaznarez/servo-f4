#ifndef _MOTOR_H
#define _MOTOR_H

/* ---------------- INCLUDES --------------- */
#include "stm32f4xx_hal_conf.h"
#include "bsp.h"

/* -------------- DEFINITIONS -------------- */
#define PWM_MAX 1024
#define PWM_MIN 0
#define SPEED_MAX 1024
#define SPEED_MIN 0

#define MOTOR_GPIO_Set(g,s) \
  HAL_GPIO_WritePin(g.port, g.pin, s);

/* ----------------- TYPES ----------------- */
typedef struct {
  TIM_HandleTypeDef* timer;
  HAL_TIM_ActiveChannel channel;
} timi_t;

typedef struct {
  GPIO_TypeDef* port;
  uint16_t pin;
} gpio_t;

typedef struct {
  gpio_t enable;
  gpio_t direction;
  gpio_t pwm;
  timi_t timi;
} motor_s;

/* --------------- PROTOTYPES -------------- */
void MOTOR_Init(void);
void MOTOR_IfInit(motor_t motor);
void MOTOR_TimerInit(TIM_HandleTypeDef* tim, TIM_TypeDef* instance);
void MOTOR_GPIO_Init(gpio_t gpio);

#endif
