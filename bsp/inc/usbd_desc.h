#ifndef __USBD_DESC__C__
#define __USBD_DESC__C__

/* Includes ------------------------------------------------------------------*/
#include "usbd_def.h"

/** Descriptor for the Usb device. */
extern USBD_DescriptorsTypeDef FS_Desc;

#endif /* __USBD_DESC__C__ */
