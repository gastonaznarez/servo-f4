#ifndef __USB_DEVICE__H__
#define __USB_DEVICE__H__

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "usbd_def.h"

/** USB Device initialization function. */
void MX_USB_DEVICE_Init(void);

#endif /* __USB_DEVICE__H__ */
