#ifndef _BSP_H
#define _BSP_H

#include <stdint.h>
#include "net.h"

typedef void* motor_t;
typedef enum {left, right} dir_t;
typedef uint16_t speed_t;

typedef void* sensor_t;

void BSP_Init(void);
void BSP_Delay(uint32_t delay);

void MOTOR_Start(motor_t motor, dir_t direction, speed_t speed);
void MOTOR_Release(motor_t motor);

void SENSOR_Start(sensor_t sensor);
void SENSOR_Stop(sensor_t sensor);

#endif
