#ifndef __USBD_CDC_IF_H__
#define __USBD_CDC_IF_H__

/* Includes ------------------------------------------------------------------*/
#include "usbd_cdc.h"


/** CDC Interface callback. */
extern USBD_CDC_ItfTypeDef USBD_Interface_fops_FS;


/** @defgroup USBD_CDC_IF_Exported_FunctionsPrototype USBD_CDC_IF_Exported_FunctionsPrototype
  * @brief Public functions declaration.
  * @{
  */

uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);

int8_t CDC_Receive_FS(uint8_t* Buf, uint32_t *Len);


#endif /* __USBD_CDC_IF_H__ */
