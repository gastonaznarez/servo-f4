#ifndef _NET_H
#define _NET_H

#include <string.h>
#include <stdint.h>

uint8_t NET_TxHandler(uint8_t* buf, uint16_t len);
uint8_t NET_RxHandler(uint8_t* buf, uint16_t len);

#endif
