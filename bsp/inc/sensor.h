#ifndef _SENSOR_H
#define _SENSOR_H

#include "stm32f4xx_hal_conf.h"
#include "bsp.h"

// Peripheral config
#define SENSOR_ADC ADC1
#define SENSOR_DMA DMA2
#define SENSOR_CHANNELS_LEN 2
// Conversion mapping
#define SENSOR_MAX_OUT (2^16)-1
#define SENSOR_MIN_OUT 0
#define SENSOR_MAX_IN (2^16)-1
#define SENSOR_MIN_IN 0
// Average config
#define SENSOR_OVERSAMPLING 10

#define SENSOR_BUF_LEN  SENSOR_CHANNELS_LEN * \
                        SENSOR_OVERSAMPLING

typedef uint16_t pos_t;

typedef struct {
  GPIO_TypeDef* port;
  uint16_t pin;
} gpios_t;

typedef struct {
  uint32_t channel;
  gpios_t gpio;
  enum { 
    SENSOR_ON,
    SENSOR_OFF
  } state;
} sensor_s;

void SENSOR_Init(void);

#endif
