#include "net.h"
#include "main.h"
#include "usbd_cdc_if.h"

const uint8_t buf_len = 124;
uint8_t net_buf[124] = {0};
uint32_t buf_end = 0;
uint32_t buf_start = 0;

inline uint8_t NET_TxHandler(uint8_t* buf, uint16_t len)
{
  return CDC_Transmit_FS(buf, len);
}

/* In the other side is a command line, so is probable that rec_len = 1 */
inline uint8_t NET_RxHandler(uint8_t* rec, uint16_t rec_len)
{
  uint16_t index = 0;
  while(index < rec_len){

    if(rec[index] == '\r'){
      // NET_TxHandler(&net_buf[buf_start], buf_end-buf_start);
      cmd_receive(&net_buf[buf_start], buf_end-buf_start);
      buf_start = buf_end;
    } else {
      net_buf[buf_end++] = rec[index];
    }
    
    if(buf_end == buf_len){
      buf_end = 0;
      buf_start = 0;
    }

    index++;
  }
  return 0;
}
