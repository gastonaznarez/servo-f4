#include "sensor.h"

extern void SENSOR_Callback(sensor_t sensor, pos_t);

uint8_t SENSOR_Converting = 0;
uint8_t SENSOR_ConvNumb = 0;

ADC_HandleTypeDef sensor_adc;
DMA_HandleTypeDef sensor_dma;
TIM_HandleTypeDef sensor_tim;

uint16_t sensor_buffer[SENSOR_BUF_LEN];

sensor_s sensors[SENSOR_CHANNELS_LEN] = {
  { 
    ADC_CHANNEL_0,
    { GPIOA, GPIO_PIN_0 },
    SENSOR_OFF
  },
  { 
    ADC_CHANNEL_1,
    { GPIOA, GPIO_PIN_1 },
    SENSOR_OFF
  }
};

sensor_t sensor_1 = &sensors[0];
sensor_t sensor_2 = &sensors[1];

void HAL_ADC_MspInit(ADC_HandleTypeDef* hadc)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(hadc->Instance==SENSOR_ADC)
  {
    __HAL_RCC_ADC1_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();

    for(uint8_t i = 0; i < SENSOR_CHANNELS_LEN; i++){
      GPIO_InitStruct.Pin = sensors[i].gpio.pin;
      GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
      GPIO_InitStruct.Pull = GPIO_NOPULL;
      HAL_GPIO_Init(sensors[i].gpio.port, &GPIO_InitStruct);
    }

    /* ADC1 DMA Init */
    sensor_dma.Instance = DMA2_Stream0;
    sensor_dma.Init.Channel = DMA_CHANNEL_0;
    sensor_dma.Init.Direction = DMA_PERIPH_TO_MEMORY;
    sensor_dma.Init.PeriphInc = DMA_PINC_DISABLE;
    sensor_dma.Init.MemInc = DMA_MINC_ENABLE;
    sensor_dma.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
    sensor_dma.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
    sensor_dma.Init.Mode = DMA_CIRCULAR;
    sensor_dma.Init.Priority = DMA_PRIORITY_LOW;
    sensor_dma.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    if (HAL_DMA_Init(&sensor_dma) != HAL_OK)
      while(1);

    __HAL_LINKDMA(hadc,DMA_Handle,sensor_dma);

    /* ADC1 interrupt Init */ 
    HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(ADC_IRQn);
  }

}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim_base)
{
  if(htim_base->Instance==TIM2)
    __HAL_RCC_TIM2_CLK_ENABLE();
}

void SENSOR_Init(void)
{

  /* DMA CLK and NVIC Init */
  __HAL_RCC_DMA2_CLK_ENABLE();
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

  __HAL_RCC_GPIOA_CLK_ENABLE(); 

  /* ADC Init */
  sensor_adc.Instance = SENSOR_ADC;
  sensor_adc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  sensor_adc.Init.Resolution = ADC_RESOLUTION_12B;
  sensor_adc.Init.ScanConvMode = ENABLE;
  sensor_adc.Init.ContinuousConvMode = ENABLE;
  sensor_adc.Init.DiscontinuousConvMode = DISABLE;
  sensor_adc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  sensor_adc.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T2_TRGO;
  sensor_adc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  sensor_adc.Init.NbrOfConversion = SENSOR_CHANNELS_LEN;
  sensor_adc.Init.DMAContinuousRequests = ENABLE;
  sensor_adc.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  if (HAL_ADC_Init(&sensor_adc) != HAL_OK)
    while(1);

  ADC_ChannelConfTypeDef sConfig = {0};
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;

  for(uint8_t i = 0; i < SENSOR_CHANNELS_LEN; i++){
    sConfig.Channel = sensors[i].channel;
    sConfig.Rank = i+1;
    if (HAL_ADC_ConfigChannel(&sensor_adc, &sConfig) != HAL_OK)
      while(1);
  }

  if(HAL_ADC_Start_DMA(&sensor_adc, (uint32_t*)sensor_buffer, SENSOR_BUF_LEN) != HAL_OK)
    while(1);

  /* TIM Init */
  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  sensor_tim.Instance = TIM2;
  sensor_tim.Init.Prescaler = 512;
  sensor_tim.Init.CounterMode = TIM_COUNTERMODE_UP;
  sensor_tim.Init.Period = 64;
  sensor_tim.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  sensor_tim.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&sensor_tim) != HAL_OK)
    while(1);

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&sensor_tim, &sClockSourceConfig) != HAL_OK)
    while(1);

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&sensor_tim, &sMasterConfig) != HAL_OK)
    while(1);
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
  if(hadc->Instance != SENSOR_ADC)
    return;
  
  if(SENSOR_ConvNumb++ < SENSOR_OVERSAMPLING - 1)
    return;

  SENSOR_ConvNumb = 0;

  for(uint8_t sen = 0; sen < SENSOR_CHANNELS_LEN; sen++){

    if(sensors[sen].state == SENSOR_OFF)
      continue;

    uint16_t avg = 0;
    for(uint8_t nbuf = 0; nbuf < SENSOR_OVERSAMPLING; nbuf++)
      avg += sensor_buffer[nbuf * SENSOR_CHANNELS_LEN + sen];
    avg /= SENSOR_OVERSAMPLING;

#if (SENSOR_MAX_OUT != SENSOR_MAX_IN && SENSOR_MIN_OUT != SENSOR_MIN_IN)
    avg = (avg - SENSOR_MIN_IN) * (SENSOR_MAX_OUT - SENSOR_MIN_OUT) /
                          (SENSOR_MAX_IN - SENSOR_MIN_IN) + SENSOR_MIN_OUT;
#endif

    SENSOR_Callback(&sensors[sen], avg);
  }

}

void SENSOR_Start(sensor_t void_sensor)
{
  sensor_s* sensor = (sensor_s*)void_sensor;
  sensor->state = SENSOR_ON;

  if(!SENSOR_Converting){
    HAL_TIM_Base_Start_IT(&sensor_tim);
    SENSOR_Converting = 1;
  }
}

void SENSOR_Stop(sensor_t void_sensor)
{
  sensor_s* sensor = (sensor_s*)void_sensor;
  sensor->state = SENSOR_OFF;

  uint8_t i = 0;
  while(i < SENSOR_CHANNELS_LEN && sensors[i++].state == SENSOR_OFF);

  if(i >= SENSOR_CHANNELS_LEN && SENSOR_Converting){
    HAL_TIM_Base_Stop_IT(&sensor_tim);
    SENSOR_Converting = 0;
  }
}
