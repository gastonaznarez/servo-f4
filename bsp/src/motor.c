/* ---------------- INCLUDES --------------- */
#include "motor.h"
#include "stm32f4xx_hal_conf.h"
#include "stm32f4xx_hal.h"

/* ---------------- GLOBALES --------------- */
motor_t motor_1;
motor_t motor_2;

TIM_HandleTypeDef htim3;

motor_s motor_1s = {
  { GPIOD, GPIO_PIN_0 },
  { GPIOD, GPIO_PIN_1 },
  { GPIOA, GPIO_PIN_6 },
  { &htim3, TIM_CHANNEL_1 }
};

motor_s motor_2s = {
  { GPIOD, GPIO_PIN_2 },
  { GPIOD, GPIO_PIN_3 },
  { GPIOA, GPIO_PIN_7 },
  { &htim3, TIM_CHANNEL_2 }
};

/* --------------- FUNCTIONS --------------- */
void MOTOR_Init(void)
{
  __HAL_RCC_GPIOA_CLK_ENABLE();    
  __HAL_RCC_GPIOD_CLK_ENABLE();    
  __HAL_RCC_TIM3_CLK_ENABLE();

  motor_1 = &motor_1s;
  motor_2 = &motor_2s;

  // Init TIMERS
  MOTOR_TimerInit(&htim3, TIM3);

  // Init MOTORS
  MOTOR_IfInit(motor_1);
  MOTOR_IfInit(motor_2);
}

// This function is called on HAL_TIM_PWM_Init
void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* htim_pwm)
{
  if(htim_pwm->Instance==TIM3)
    __HAL_RCC_TIM3_CLK_ENABLE();
}

void MOTOR_TimerInit(TIM_HandleTypeDef* tim, TIM_TypeDef* instance)
{
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  tim->Instance = instance;
  tim->Init.Prescaler = 128;
  tim->Init.CounterMode = TIM_COUNTERMODE_UP;
  tim->Init.Period = 1024;
  tim->Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  tim->Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(tim) != HAL_OK)
    while(1);

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(tim, &sMasterConfig) != HAL_OK)
    while(1);
}

void MOTOR_IfInit(motor_t void_motor)
{
  motor_s* motor = (motor_s*)void_motor;

  TIM_OC_InitTypeDef sConfigOC = {0};

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(motor->timi.timer, &sConfigOC, motor->timi.channel) != HAL_OK)
    while(1);

  GPIO_InitTypeDef GPIO_InitStruct;

  GPIO_InitStruct.Pin = motor->pwm.pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
  HAL_GPIO_Init(motor->pwm.port, &GPIO_InitStruct);

  // INIT ENABLE AND DIRECTION GPIOs
  MOTOR_GPIO_Init(motor->enable);
  MOTOR_GPIO_Init(motor->direction);
}

void MOTOR_GPIO_Init(gpio_t gpio)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
    
  HAL_GPIO_WritePin(gpio.port, gpio.pin, GPIO_PIN_RESET);

  GPIO_InitStruct.Pin = gpio.pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(gpio.port, &GPIO_InitStruct);
}

void MOTOR_Start(motor_t void_motor, dir_t direction, speed_t speed)
{
  motor_s* motor = (motor_s*)void_motor;

  // Set motor on freerunning
  MOTOR_GPIO_Set(motor->enable, GPIO_PIN_RESET);

#if (PWM_MAX != SPEED_MAX && PWM_MIN != SPEED_MIN)
  // Map the speed value
  uint16_t new_speed = (speed - SPEED_MIN) * (PWM_MAX - PWM_MIN) /
                          (SPEED_MAX - SPEED_MIN) + PWM_MIN;
#else
  uint16_t new_speed = speed;
#endif

  if(direction == left){
    MOTOR_GPIO_Set(motor->direction, GPIO_PIN_RESET);
  } else {
    MOTOR_GPIO_Set(motor->direction, GPIO_PIN_SET);
    // Invert the pwm pulse
    new_speed = PWM_MAX - new_speed;
  }

  // Set PWM value
  TIM_OC_InitTypeDef sConfigOC;
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = new_speed;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  HAL_TIM_PWM_ConfigChannel(motor->timi.timer, &sConfigOC, motor->timi.channel);
  HAL_TIM_PWM_Start(motor->timi.timer, motor->timi.channel);  

  MOTOR_GPIO_Set(motor->enable, GPIO_PIN_SET);
}

void MOTOR_Release(motor_t void_motor)
{
  motor_s* motor = (motor_s*)void_motor;
  MOTOR_GPIO_Set(motor->enable, GPIO_PIN_RESET);
}
