# APP
SRCS = main.c cmd.c pid.c servo.c

# BSP
SRCS += bsp.c net.c motor.c sensor.c

# System
SRCS += stm32f4xx_it.c system_stm32f4xx.c

# USB
SRCS += usbd_conf.c usbd_cdc_if.c usbd_desc.c usb_device.c

# Utilities
# SRCS += stm324xg_eval_sdio_sd.c stm324xg_eval.c

# FreeRTOS
# SRCS += port.c list.c queue.c tasks.c event_groups.c timers.c heap_4.c frtos.c stream_buffer.c

# Project name
PROJ_NAME=servo
OUTPATH=build

###################################################

BINPATH=~/sat/bin
CC=arm-none-eabi-gcc
OBJCOPY=arm-none-eabi-objcopy
SIZE=arm-none-eabi-size

CFLAGS  = -std=gnu99 -g -O0 -Wall -TSTM32F417VETx_FLASH.ld -specs=nano.specs -specs=nosys.specs
CFLAGS += -mlittle-endian -mthumb -mthumb-interwork -mcpu=cortex-m4

CFLAGS += -fsingle-precision-constant -Wdouble-promotion
CFLAGS += -mfpu=fpv4-sp-d16 -mfloat-abi=hard
CFLAGS += -Wl,--gc-sections

###################################################

vpath %.c app/src
vpath %.c bsp/src

ROOT=$(shell pwd)

# Includes
CFLAGS += -Ibsp/inc -Iapp/inc
CFLAGS += -Ilib/CMSIS/Device/ST/STM32F4xx/Include
CFLAGS += -Ilib/CMSIS/Include
CFLAGS += -Ilib/Conf

# Library paths
LIBPATHS  = -Llib/HAL
LIBPATHS += -Llib/USB

# Libraries to link
LIBS = -lm -lhal -lusb
CFLAGS += -DSTM32F417xx

# Extra includes
CFLAGS += -Ilib/HAL/inc
CFLAGS += -Ilib/USB/Class/CDC/Inc
CFLAGS += -Ilib/USB/Core/Inc

# add startup file to build
SRCS += bsp/src/startup_stm32f407xx.s

OBJS = $(SRCS:.c=.o)

ST_FLASH ?= st-flash
###################################################

.PHONY: lib proj

all: lib proj
	$(SIZE) $(OUTPATH)/$(PROJ_NAME).elf

lib:
	if [ ! -d $(OUTPATH) ]; then mkdir $(OUTPATH); fi
	$(MAKE) -C lib FLOAT_TYPE=$(FLOAT_TYPE)

proj: 	$(OUTPATH)/$(PROJ_NAME).elf

$(OUTPATH)/$(PROJ_NAME).elf: $(SRCS)
	$(CC) $(CFLAGS) $^ -o $@ $(LIBPATHS) $(LIBS)
	$(OBJCOPY) -O ihex $(OUTPATH)/$(PROJ_NAME).elf $(OUTPATH)/$(PROJ_NAME).hex
	$(OBJCOPY) -O binary $(OUTPATH)/$(PROJ_NAME).elf $(OUTPATH)/$(PROJ_NAME).bin

clean:
	rm -f *.o
	rm -f $(OUTPATH)/$(PROJ_NAME).elf
	rm -f $(OUTPATH)/$(PROJ_NAME).hex
	rm -f $(OUTPATH)/$(PROJ_NAME).bin
	# $(MAKE) clean -C lib # Remove this line if you don't want to clean the libs as well
	rm -r $(OUTPATH)
flash:
	make && $(ST_FLASH) write $(OUTPATH)/$(PROJ_NAME).bin 0x8000000
