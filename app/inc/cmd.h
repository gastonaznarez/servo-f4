#ifndef _CMD_H
#define _CMD_H

#include <stdlib.h>
#include <stdint.h>

#define CMD_LIST_MIN_SIZE 2
#define CMD_QUEUE_SIZE 8

#define IS_VALID_CHAR(c) (c != 0 && c != '\r' && c != '\n' && \
                          c != '\n' && c != ' ')
#define IS_INVALID_CHAR(c) (c == 0 || c == '\r' || c == '\n' || \
                            c == '\n' || c ==' ')

typedef struct {
  char* queue[CMD_QUEUE_SIZE];
  uint16_t start;
  uint16_t end;
}cmd_queue_s;

typedef cmd_queue_s* cmd_queue_t;

typedef void (*cmd_fun_t)(char**, uint16_t);

typedef struct {
  cmd_fun_t fun;
  char* name;
  uint16_t name_len;
} cmd_s;

typedef cmd_s* cmd_t;

typedef struct {
  uint16_t list_len;
  uint16_t list_used;
  cmd_fun_t invalid_cmd;
  cmd_t* list;
  cmd_queue_t queue;
} cmd_if_s;

typedef cmd_if_s* cmd_if_t;

typedef struct {
  char* cmd;
  char** argv;
  uint16_t argc;
} cmd_line_s;

typedef cmd_line_s* cmd_line_t;

cmd_if_t CMD_Init(uint16_t len, cmd_fun_t invalid_cmd);
uint8_t CMD_NewCommand(cmd_if_t cmd_if, cmd_fun_t cmd, char* name, uint16_t name_len);
uint8_t CMD_RxHandler(cmd_if_t cmd_if, char* line, uint16_t len);
uint8_t CMD_RunCommand(cmd_if_t cmd_if);

#endif

/* DOCUMENTATION */
// TODO
