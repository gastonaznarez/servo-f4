#ifndef _SERVO_H
#define _SERVO_H

#include "main.h"

#define SERVO_DT 1.0f
#define SERVO_KP 1.0f
#define SERVO_KI .1f
#define SERVO_KD .0f
#define SERVO_PID_MAX 1023.0f
#define SERVO_PID_MIN (-1023.0f)
#define SERVOS_LEN 2
#define SERVO_HYSTERESIS 15

#define SERVO_ON 1
#define SERVO_OFF 0

#define ABS(x) ((x >= 0) ? x : (-x))

typedef uint16_t pos_t;

typedef struct {
  motor_t motor;
  sensor_t sensor;
  pid_if_t pid;
  pos_t setpoint;

  pos_t last_pos;
  uint8_t status;
} servo_t;

void SERVO_GoTo(uint8_t servo, pos_t position);
void SERVO_Init(void);
void SERVO_StopAll(void);

#endif
