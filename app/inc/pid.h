#ifndef _PID_H
#define _PID_H

#include <stdlib.h>
#include <float.h>

// Not using ifndef ... endif because conflicts
#define PID_MIN(x,y) ((x > y) ? y : x)
#define PID_MAX(x,y) ((x < y) ? y : x)

// PRESITION = { FLOAT, DOUBLE, LONGDOUBLE }
#define PID_PRECISION FLOAT

#if PID_PRECISION == DOUBLE
  #define PID_FLOAT_MAX DBL_MAX
  #define PID_FLOAT_MIN DBL_MIN
  typedef double pid_float_t;
#elif PID_PRECISION == LONGDOUBLE
  #define PID_FLOAT_MAX LDBL_MAX
  #define PID_FLOAT_MIN LDBL_MIN
  typedef long double pid_float_t;
#else
  #define PID_FLOAT_MAX FLT_MAX
  #define PID_FLOAT_MIN FLT_MIN
  typedef float pid_float_t;
#endif

typedef struct {
  // Tunning
  pid_float_t dt;
  pid_float_t kp;
  pid_float_t ki;
  pid_float_t kd;
  // Bounds
  pid_float_t out_max;
  pid_float_t out_min;
  // lookback values
  pid_float_t error;
  pid_float_t integral;
} pid_if_s;

typedef pid_if_s* pid_if_t;

typedef enum { pid_dt, pid_kp, pid_ki, pid_kd } pid_if_tune_t;

pid_if_t PID_Create(pid_float_t dt, pid_float_t kp, pid_float_t ki, pid_float_t kd);
void PID_Delete(pid_if_t pid);
void PID_Tune(pid_if_t pid, pid_if_tune_t param, pid_float_t value);
void PID_SetLimits(pid_if_t pid, pid_float_t out_min, pid_float_t out_max);
void PID_Reset(pid_if_t pid);
pid_float_t PID_Compute(pid_if_t pid, pid_float_t setpoint, pid_float_t readed);

#endif
