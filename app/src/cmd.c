#include "cmd.h"

// Some day implement dynamic size list
cmd_if_t CMD_Init(uint16_t len, cmd_fun_t invalid_cmd)
{
  cmd_if_t cmd_if = calloc(1, sizeof(cmd_if_s));
  if(cmd_if == NULL) return NULL;

  cmd_if->list = calloc(len, sizeof(cmd_t));
  if(cmd_if->list == NULL){
    free(cmd_if);
    return NULL;
  }

  cmd_if->list_len = len;

  cmd_if->queue = calloc(1, sizeof(cmd_queue_s));
  if(cmd_if->queue == NULL){
    free(cmd_if->list);
    free(cmd_if);
    return NULL;
  }

  cmd_if->invalid_cmd = invalid_cmd;

  return cmd_if;
}

uint8_t CMD_NewCommand(cmd_if_t cmd_if, cmd_fun_t cmd_fun, char*name, uint16_t name_len)
{
  if(cmd_if->list_used == cmd_if->list_len)
    return 2;

  cmd_t new_cmd = calloc(1, sizeof(cmd_s));
  if(new_cmd == NULL) return 1;

  new_cmd->name = calloc(name_len+1, sizeof(char));
  if(new_cmd->name == NULL){
    free(new_cmd);
    return 1;
  }

  new_cmd->fun = cmd_fun;
  new_cmd->name_len = name_len;

  for(uint8_t c = 0; c < name_len; c++)
    new_cmd->name[c] = name[c];
  new_cmd->name[name_len] = 0;

  cmd_if->list[cmd_if->list_used++] = new_cmd;

  return 0;
}

uint8_t CMD_RxHandler(cmd_if_t cmd_if, char* line, uint16_t len)
{
  if(len == 0)
    return 2;

  cmd_queue_t queue = cmd_if->queue;
  if(queue->start == (queue->end+1)%CMD_QUEUE_SIZE)
    return 1;

  char** qlist = queue->queue;
  qlist[queue->end] = calloc(len+1, sizeof(uint8_t));
  for(uint16_t l = 0; l < len; l++)
    qlist[queue->end][l] = line[l];
  qlist[queue->end][len] = 0;
  queue->end = (queue->end+1)%CMD_QUEUE_SIZE;

  return 0;
}

char* CMD_PopQueue(cmd_if_t cmd_if)
{
  cmd_queue_t queue = cmd_if->queue;
  char** list = cmd_if->queue->queue;

  if(queue->start == queue->end)
    return NULL;

  // TODO: check on the binary that the order not chage
  // in this way, this is not a critical section
  char* element = list[queue->start];
  queue->start = (queue->start+1)%CMD_QUEUE_SIZE;

  return element;
}

void CMD_Execute(cmd_if_t cmd_if, char* cmd, char** argv, uint16_t argc)
{
  cmd_t* list = cmd_if->list;
  for(uint16_t index = 0; index < cmd_if->list_used; index++){
    uint16_t i = 0;
    char* name = list[index]->name;
    while(cmd[i] != 0 && cmd[i] == name[i]) i++;
    if(cmd[i] == name[i]){
      list[index]->fun(argv, argc);
      return;
    }
  }
  cmd_if->invalid_cmd(argv, argc);
}

uint8_t CMD_RunCommand(cmd_if_t cmd_if)
{
  char* cmd = CMD_PopQueue(cmd_if);
  if(cmd == NULL)
    return 1;

  uint16_t len = 0;
  while(cmd[len] != 0) len++;
  uint16_t start = 0;

  if(len == 0)
    return 1;

  // Remove spaces on the start
  while(len-start > 0 && IS_INVALID_CHAR(cmd[start])) start++;
  cmd = &cmd[start];
  len -= start;

  uint16_t argc = 0;
  for(uint16_t k = 0; k < len; k++){
    if(!IS_VALID_CHAR(cmd[k]))
      cmd[k] = 0;
    else if(k > 0 && cmd[k-1] == 0) argc++;

  }

  // Remove EOL characters
  while(len > 1 && IS_INVALID_CHAR(cmd[len-1]) && IS_INVALID_CHAR(cmd[len])) len--;

  if(len == 0)
    return 1;

  // Create argv
  // What hapends when argc is 0?
  char* argv[argc];

  uint16_t index = 1;
  for(uint16_t j = 0; j < argc; j++){
    while(!(cmd[index-1] == 0 && cmd[index] != 0)) index++;
    argv[j] = &cmd[index++];
  }

  // Run cmd
  CMD_Execute(cmd_if, cmd, argv, argc);

  free(cmd-start);
  return 0;
}
