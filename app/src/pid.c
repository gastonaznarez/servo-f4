#include "pid.h"

pid_if_t PID_Create(pid_float_t dt, pid_float_t kp, pid_float_t ki, pid_float_t kd)
{
  pid_if_t pid = calloc(1, sizeof(pid_if_s));
  if(pid == NULL)
    return NULL;

  pid->dt = dt;
  pid->kp = kp;
  pid->ki = ki;
  pid->kd = kd;

  pid->out_min = PID_FLOAT_MIN;
  pid->out_max = PID_FLOAT_MAX;

  // Calloc already set this on 0, but...
  pid->error = 0;
  pid->integral = 0;

  return pid;
}

void PID_Delete(pid_if_t pid)
{
  if(pid != NULL)
    free(pid);
}

void PID_Tune(pid_if_t pid, pid_if_tune_t param, pid_float_t value)
{
  switch(param){
    case pid_dt:
      pid->dt = value;
      break;
    case pid_kp:
      pid->kp = value;
      break;
    case pid_ki:
      pid->ki = value;
      break;
    case pid_kd:
      pid->kd = value;
      break;
  }
}

void PID_SetLimits(pid_if_t pid, pid_float_t out_min, pid_float_t out_max)
{
  pid->out_min = out_min;
  pid->out_max = out_max;
}

void PID_Reset(pid_if_t pid)
{
  pid->error = 0;
  pid->integral = 0;
}

pid_float_t PID_Compute(pid_if_t pid, pid_float_t setpoint, pid_float_t readed)
{
  // PROPORTIONAL TERM
  pid_float_t error = setpoint - readed;

  // DERIVATIVE TERM
  pid_float_t deriv = (error - pid->error)/pid->dt;

  // INTEGRAL TERM
  pid->integral += error*pid->dt;

  pid->error = error;

  // CALCULATE
  pid_float_t out = error*pid->kp + pid->integral*pid->ki + deriv*pid->kd;

  out = PID_MAX(out, pid->out_min);
  out = PID_MIN(out, pid->out_max);

  return out;
}
