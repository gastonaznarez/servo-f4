#include "main.h"
#include <strings.h>
#include <stdlib.h>
#include <stdio.h>


extern servo_t servos[2];
volatile cmd_if_t cmd;

char* tofew = "To/Few arguments\r\n";
char* badargs = "Bad arguments\r\n";
char* info_header = "Servo | Status | Last value\r\n------+--------+-----------\r\n";
char* msg_help = "\r\nWelcome: This is a help message...\r\n"
                 "Board: STM32F407VET6\r\n"
                 "Commands:\r\n\thelp\r\n\t\tPrint this message.\r\n\t"
                 "stop\r\n\t\tStop all servo's motors.\r\n\r\t"
                 "goto [servo number] [1..270 position]\r\n\t\t"
                 "Set position of the servo.\r\n\t"
                 "info\r\n\t\tGet servo's info.\r\n\r\n"

                 "Bye...\r\n\r\n";

void cmd_GoTo(char** argv, uint16_t argc)
{
  if(argc != 2){
    NET_TxHandler((uint8_t*)tofew, 18);
    return;
  }

  float value = atof(argv[1]);

  if(value < 0 || value > 270){
    NET_TxHandler((uint8_t*)badargs, 15);
    return;
  }

  uint16_t mapped = value * (4095 / 270);

  if((char)argv[0][0] == '1')
    SERVO_GoTo(0, mapped);
  else if((char)argv[0][0] == '2')
    SERVO_GoTo(1, mapped);
  else {
    NET_TxHandler((uint8_t*)badargs, 15);
    return;
  }

  NET_TxHandler((uint8_t*)"OK...\r\n", 7);
}

void cmd_ServosStop(char** argv, uint16_t argc)
{
  SERVO_StopAll();
  NET_TxHandler((uint8_t*)"All servos stoped\r\n", 19);
}

void cmd_Info(char** argv, uint16_t argc)
{
  NET_TxHandler((uint8_t*)info_header, strlen(info_header));
  
  for(uint8_t j = 0; j < 2; j++){
    BSP_Delay(100);
    char str[65] = {0};
    strcat(str, "   ");
    itoa(j, &str[strlen(str)], 10);
    strcat(&str[(strlen(str))], "  |");
    if(((servo_t)servos[j]).status == SERVO_ON)
      strcat(&str[(strlen(str))], " MOVING | ");
    else
      strcat(&str[(strlen(str))], "  STOP  | ");
    uint32_t mapped = (servos[j].last_pos * 270)/4095;
    itoa(mapped, &str[strlen(str)], 10);
    strcat(&str[(strlen(str))], "\r\n\0");
    NET_TxHandler((uint8_t*)str, strlen(str));
  }
    BSP_Delay(100);
}

void cmd_invalid(char** argv, uint16_t argc)
{
  NET_TxHandler((uint8_t*)"INVALID\r\n", 9);
  return;
}

// This function is called on USB_RxHandler when receive EOL
void cmd_receive(uint8_t* rec, uint32_t rec_len)
{
  CMD_RxHandler(cmd, (char*) rec, rec_len);
}

void cmd_Help(char** argv, uint16_t argc)
{
  NET_TxHandler((uint8_t*)msg_help, strlen(msg_help));
  BSP_Delay(1000);
}

int main(void)
{
  cmd = CMD_Init(4, cmd_invalid);
  CMD_NewCommand(cmd, cmd_GoTo, "goto", 4);
  CMD_NewCommand(cmd, cmd_ServosStop, "stop", 4);
  CMD_NewCommand(cmd, cmd_Info, "info", 4);
  CMD_NewCommand(cmd, cmd_Help, "help", 4);

  BSP_Init();
  SERVO_Init();
  
  while(1){
    BSP_Delay(50);
    CMD_RunCommand(cmd);
  }
}
