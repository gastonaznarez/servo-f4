#include "servo.h"

extern motor_t motor_1;
extern motor_t motor_2;
extern sensor_t sensor_1;
extern sensor_t sensor_2;

servo_t servos[SERVOS_LEN] = { {0}, {0} };

void SERVO_Init(void)
{
  servos[0].sensor = sensor_1;
  servos[0].motor = motor_1;
  servos[1].sensor = sensor_2;
  servos[1].motor = motor_2;

   for(uint8_t i = 0; i < SERVOS_LEN; i++){
     servos[i].pid = PID_Create(SERVO_DT, SERVO_KP, SERVO_KI, SERVO_KD);
     PID_SetLimits(servos[i].pid, SERVO_PID_MIN, SERVO_PID_MAX);
   }
}

void SERVO_StopAll(void)
{
  for(uint8_t i = 0; i < SERVOS_LEN; i++){
    SENSOR_Stop(servos[i].sensor);
    MOTOR_Release(servos[i].motor);
    PID_Reset(servos[i].pid);
    servos[i].status = SERVO_OFF;
  }
}

void SENSOR_Callback(sensor_t sensor, uint16_t value){
  uint8_t servo = 0;
  while(servos[servo].sensor != sensor && servo < SERVOS_LEN) servo++;

  if(servo == SERVOS_LEN) return;

  if(abs(value - servos[servo].setpoint) < SERVO_HYSTERESIS){
    SENSOR_Stop(servos[servo].sensor);
    MOTOR_Release(servos[servo].motor);
    PID_Reset(servos[servo].pid);
    servos[servo].status = SERVO_OFF;
    return;
  }

  float pid_res = PID_Compute(servos[servo].pid, (float)servos[servo].setpoint, (float)value);
  dir_t dir = (pid_res >= 0) ? right : left;
  
  uint16_t speed = (uint16_t)ABS(pid_res);

  MOTOR_Start(servos[servo].motor, dir, speed);
  servos[servo].last_pos = value;
}

void SERVO_GoTo(uint8_t servo, pos_t position)
{
  servos[servo].setpoint = position;
  servos[servo].status = SERVO_ON;
  SENSOR_Start(servos[servo].sensor);
}
